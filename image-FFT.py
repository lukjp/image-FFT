'''
Apply the discrete fourier transformation on one ore more directions of an image
- only in black and white (B/W) - and store the resulting image. Additionally
the amplitude is displayed logarithmically in an B/W image.
'''
from PIL import Image
import numpy as np
from math import sin,cos,tau
import cmath
import sys

FFT_Axes = (1,) # List of axes, right is 1, down is 0

# Open orignal image, convert it to B/W and save it in B/W.
origImg = Image.open(sys.argv[1])
BWImg = origImg.convert(mode='L')
BWImg.save(sys.argv[1]+'.BW.png')
data = np.array(BWImg)


# Function to convert an array of complex numbers to HSV (hue, saturation,
# value / brightness).
# - The complex phase of the numbers corresponds to the hue.
# - The absolute value of the numbers corresponds to the brightness, but is
#   normalized: The highest number gets maximum brightness.
# - Saturation is always maximal.
def ComplexToHSV(data):
    dataAbs = np.absolute(data)
    dataAbs = dataAbs / dataAbs.max() * 256
    np.clip(dataAbs, a_min = 0, a_max = 255, out = dataAbs)
    np.floor(dataAbs, out = dataAbs)
    Value = np.zeros_like(data, dtype=np.uint8)
    Value = dataAbs.astype(dtype=np.uint8)
    
    dataPhase = 256 * np.angle(data) / tau
    Hue = dataPhase.astype(dtype=np.uint8)
    
    Sat = np.full_like(data, 255, dtype=np.uint8)
    
    HSV = np.transpose(np.array([Hue,Sat,Value]), (1,2,0))
    
    return HSV

# calculate the fourier transform of the image
dataFourier = np.fft.rfftn(data, axes=FFT_Axes)
# create an image by converting the complex numbers to a color
imgF = Image.fromarray(ComplexToHSV(dataFourier), 'HSV')
# change image to RGB because png can't handle HSV and save it
imgF = imgF.convert(mode='RGB')
imgF.save(sys.argv[1]+'.fourier.png')

# calculate the logarithm of the amplitudes (0 gets 0 again)
dataFourierLog = np.ma.log(np.abs(dataFourier))
dataFourierLog = dataFourierLog.filled(0)

# scale it to use maximum contrast
dataFourierLog = ( (dataFourierLog - np.min(dataFourierLog)) * 
                    255 / (np.max(dataFourierLog) - np.min(dataFourierLog) ) )
# generate and save B/W image
imgFLog = Image.fromarray(dataFourierLog.astype(np.uint8), 'L')
imgFLog.save(sys.argv[1]+'.fourier-log.png')
